

# packages ----------------------------------------------------------------

require(hrjexport)
require(ctctools)


# function defs -----------------------------------------------------------

readHrjFiles2 <- function(hrj_files){
	calendar_hrj <- NULL
	brood_hrj <- NULL
	if (length(hrj_files) > 0) {
		for (hrj_idx in 1:length(hrj_files)) {
			hrj_file_name <- hrj_files[hrj_idx]
			cat(paste0("Reading hrj file: ", hrj_file_name, 
								 "\n"))
			
			hrj_df <- hrjexport:::readSingleHrjFile(hrj_file_name)
			if (grepl("c1.hrj$", hrj_file_name, ignore.case = TRUE)) {
				calendar_hrj <- dplyr::bind_rows(calendar_hrj, hrj_df)
			}
			else if (grepl("b1.hrj$", hrj_file_name, ignore.case = TRUE)) {
				brood_hrj <- dplyr::bind_rows(brood_hrj, hrj_df)
			}
			else {
				stop(paste0("Unknown brood/calendar year HRJ: \"", 
										hrj_file_name, "\" in \"", zip_files[zip_idx], 
										"\""))
			}
		}
	}
	return(list(calendar_hrj = calendar_hrj, brood_hrj = brood_hrj))
	
}





# data: settings --------------------------------------------------------------------

settings <- yaml::read_yaml("user_settings.config")

# data: text import -------------------------------------------------------

data <- list()

hrj.filenames <- list.files(settings$path.coshak.out, "hrj$", ignore.case = TRUE, full.names = TRUE, recursive = TRUE)
data$hrj <- readHRJtext(hrj.filenames)

str(data$hrj$hrj.cwt.list$HRJ_CY)

hrj.list.long <- reshapeHRJtolong.basic(data$hrj$hrj.cwt.list)

setdiff(data$hrj$hrj.cwt.list$HRJ_CY$StockID, hrj.list.long$HRJ_CY$StockID)

#this creates the so-called 'p' table which is the combined c & b tables
#run ?ctctools::updateHRJ_ctable  to get details on combining method
hrj.list.long.update <- updateHRJ_ctable(hrj.list.long)

hrj.list.long.update$HRJ_CYrevised <- hrj.list.long.update$HRJ_CYrevised[! is.na(hrj.list.long.update$HRJ_CYrevised$value),]


# data:import hrjexport package -------------------------------------------------------------

hrj.filenames <- list.files(settings$path.coshak.out, "hrj$", ignore.case = TRUE, full.names = TRUE, recursive = TRUE)

hrj_list <- readHrjFiles2(hrj.filenames)


data.hrjexport <- combineHrjData(hrj_list, inc_method_col = TRUE)
data.hrjexport <- as.data.frame(data.hrjexport)
names(data.hrjexport)

names(data.hrjexport)[4:8] <- c("AEQCat", "AEQTot", "NomCat", "NomTot", "Pop")
names(data.hrjexport)[names(data.hrjexport)=="brood_year"] <- "broodyear"
names(data.hrjexport)[names(data.hrjexport)=="fishery_id"] <- "fisheryindex"
names(data.hrjexport)[names(data.hrjexport)=="stock_code"] <- "StockID"
names(data.hrjexport)[names(data.hrjexport)=="age"] <- "age.true"

varying <- c("AEQCat","AEQTot","NomCat","NomTot","Pop")
data.hrjexport.long <- reshape(data.hrjexport[,c("StockID", "broodyear","fisheryindex","age.true","AEQCat","AEQTot"
																								 ,"NomCat","NomTot","Pop", "method" )], direction = "long", varying = list(varying), idvar=c("StockID", "broodyear","fisheryindex","age.true", "method"), timevar = "data.type", times = varying, v.names = "value.hrjexport")



# data:combine imports ----------------------------------------------------


nrow(hrj.list.long.update$HRJ_CYrevised)
nrow(data.hrjexport.long)

setdiff(data.hrjexport.long$StockID, hrj.list.long.update$HRJ_CYrevised$StockID)
setdiff( hrj.list.long.update$HRJ_CYrevised$StockID, data.hrjexport.long$StockID)

data.merge <- merge(hrj.list.long.update$HRJ_CYrevised[,c("fisheryindex", "broodyear","data.type","age","age.true","StockID", "value","value.b","value.c", "data.from.b")], data.hrjexport.long, sort = FALSE, all=TRUE)

nrow(data.merge)

unique(data.merge$StockID[is.na(data.merge$value)])
nrow(data.merge[is.na(data.merge$value.hrjexport),])
View(data.merge[is.na(data.merge$value.hrjexport),])

#check cases where output doesn't agree
nrow(data.merge[!is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport,])
View(data.merge[!is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport,])

#check cases where output agrees
nrow(data.merge[data.merge$data.from.b==TRUE & !is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value ==data.merge$value.hrjexport,])
View(data.merge[data.merge$data.from.b==TRUE & data.merge$method=="Brood" & !is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value ==data.merge$value.hrjexport & data.merge$value !=0,])

View(data.merge[data.merge$data.from.b==FALSE & data.merge$method=="Calendar"  & !is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport,])

unique(data.merge$StockID[!is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport])

table(data.merge$StockID[!is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport])

data.merge$returnyear <- data.merge$broodyear+data.merge$age.true

data.tmp <- data.merge[!is.na(data.merge$value) & !is.na(data.merge$value.hrjexport) & data.merge$value !=data.merge$value.hrjexport & data.merge$StockID=="BQR" & data.merge$broodyear==2006 ,]

data.tmp <- data.tmp[order(data.tmp$StockID, data.tmp$data.type, data.tmp$fisheryindex, data.tmp$returnyear,  data.tmp$age),]
View(data.tmp[,c("StockID", "data.type", "fisheryindex", "broodyear","returnyear", "age", "age.true", "data.from.b","value", "value.b", "value.c", "method", "value.hrjexport")])
View(unique(data.tmp[,c("StockID", "returnyear", "broodyear", "age", "age.true", "data.from.b", "data.type", "value.c")]))


writeClipboard( knitr::kable(data.tmp[row.names(data.tmp)== 498540,], row.names = F))


writeClipboard( knitr::kable(hrj.list.long.update$HRJ_CYrevised[hrj.list.long.update$HRJ_CYrevised$StockID=="BQR" & hrj.list.long.update$HRJ_CYrevised$returnyear==2008 & hrj.list.long.update$HRJ_CYrevised$fisheryindex==1 & hrj.list.long.update$HRJ_CYrevised$data.type=="AEQTot", ], row.names = F))

								
